
"""
(c) Wishva Herath
pyBootstrapHTML (version 1) help the creation of bootstrap HTML using python. Version 2 will have the ability to create other functions.


Version 1.1 FORMS + ALERTS

Version 1.0 CREATING FORMS
It takes in a string formated according to the doc_string_guide and convers it into bootstrap HTML.

doc_string_guide
----------------
    i. var_name: description [ age : age of the person]
    ii. var_name: type, description [age: int, age of person]
    iii. var_name: type, default value, description [age: int, default = 40, age of person ]
    iv. var_name: type, default value, range, description [age: int, default = 40, 0 to 100, age of person]
    v. var_name, type, range, description [age:int,  0 to 100, age of person]
    v1. var_name, default, options, description [age: default = 40, options = 10 | 20 | 30 | 40, age of person]


example
--------
text = "project_name: string, Name of the project
    projectlet: string, Name of the local Network
    description: string, Project Description"

"""
from flask import url_for



def textToBHtmlForm(text,url, popup, modal_title = None, modal_name = "modal1"):
    """
    
    Takes in a string which is formated according to the (above mentioned) doc_string_guide and converts it into a Bootstrap HTML form.
    
    Parameters
    ----------
    text: the doc_string_guide formated string
    url: the POST url of the form
    popup: bool, will the form be a popup?
    
    Returns
    -------
    html_string: the generated html string
    script: optional, only if popup == True. The javascript code to automatically activate the modal
    
    """
    
    keys, type_dict, default_dict, options_dict, description_dict = textToInterfaceDict(text)
    print default_dict, "nnnn"
    html_string =  dictsToBHTML(keys, type_dict, default_dict,options_dict, description_dict, url)
    
    if popup:
        html_string, script = htmlToPopUpHTML(html_string, modal_title, modal_name)
        return html_string,script
    else:
        return html_string
    
    
    
    



def textToInterfaceDict(text):
    
    """
    Takes in a text formated according to doc_string_guide and converts it to an interface_dicts.
    
    Parameters
    ----------
    text: the doc_string_guide formated string

    Returns
    -------
    keys: keys of the dictionary. This is the first value that occurs on the left of ":".
    type_dict: type 
    default_dict: default value
    options_dict: options
    description_dict: description
    
    """
    
    doc = text #doc is used for text because it was first written to use the doc string to generate ipython interfaces
    inp = text.split("\n")

    
    #processing variables
    full_dict = {} #{var_name: the rest of the line}
    type_dict = {} #{var_name:type}
    default_dict = {}
    options_dict = {}
    description_dict = {} #{var_name:description}
    
    #inp
    info_lines =[x.strip() for x in inp if ':' in x]
    #print info_lines
    keys = []
    #filling full_dict
    for l in info_lines:
        k = l.split(':')[0].strip()
        keys.append(k)
        rest = l.split(':')[1].strip()
        #print k,'..',rest, len(rest.split(','))
        
        rest_string = rest.split(',')
        comma_count = len(rest_string)
        
        if comma_count == 0 :
            print 'Error in ', k
        
        elif comma_count == 1:
            #type
            type_dict[k] = rest_string[0].strip()
            default_dict[k] = '_'
            options_dict[k] = '_'
            description_dict[k] = '_'
            
        elif comma_count == 2:
            #type, description
            type_dict[k] = rest_string[0].strip()
            default_dict[k] = '_'
            options_dict[k] = '_'
            description_dict[k] = rest_string[1].strip()
            
            
            
        elif comma_count == 3:
            if '|' in rest:
                #type, options, description
                type_dict[k] = rest_string[0].strip()
                default_dict[k] = '_'
                options_dict[k] = rest_string[1].strip()
                description_dict[k] = rest_string[2].strip()
            
            else:
                #type, default, description
                type_dict[k] = rest_string[0].strip()
                default_dict[k] = rest_string[1].strip()
                options_dict[k] = '_'
                description_dict[k] = rest_string[2].strip()
                
        elif comma_count == 4:
            #type, default, options, description
            type_dict[k] = rest_string[0].strip()
            default_dict[k] = rest_string[1].strip()
            options_dict[k] = rest_string[2].strip()
            description_dict[k] = rest_string[3].strip()
            
        else:
            #type, default,options, description, OTHER STUFF TO IGNORE
            type_dict[k] = rest_string[0].strip()
            default_dict[k] = rest_string[1].strip()
            options_dict[k] = rest_string[2].strip()
            description_dict[k] = rest_string[3].strip()
            
    return keys, type_dict, default_dict, options_dict, description_dict


def beautifyName(display_name):
    """
    #beutifyName old name
    This attempts to beautify the display field names.
    
    1. user_name --> User Name
    2. name --> Name
    3. userName --> User Name
    
    Parameters
    ----------
    display_name: str, text to be changed
    
    Returns
    -------
    display_name_nice: prettified display name
    
    """
    dn = ""
    if "_" in display_name:
        t = display_name.strip().split('_')
        
        for i in t:
            dn += i.strip() + " "
        dn = dn.strip() #removes the trailing space
        
        dn2 = ""
        if dn == "":
            dn = display_name
        t = dn.split()
        for i in t:
            dn2 += i.title() + " "
        
        return dn2.strip()
        
    else:
    
        #(3)
        display_name_nice = display_name
        display_name_nice_2 = ""
        for character in display_name_nice:
            if character.upper() == character and character != " ":
                print character, 'upper'
                #then its an upper case character
                display_name_nice_2  = display_name_nice_2 + " " + character
            elif character != " ":
                display_name_nice_2 = display_name_nice_2 + character
    	
    		
    		
    	
    
        return display_name_nice_2.strip().title()
        
    


def dictsToBHTML(k, type_dict, default_dict,options_dict, description_dict, url):
    
    """
    Takes the series of interface_dicts and creates a Bootstrap HTML form.
    
    Parameters
    ----------
    keys: keys of the dictionary. This is the first value that occurs on the left of ":".
    type_dict: type 
    default_dict: default value
    options_dict: options
    description_dict: description
    
    Returns
    --------
    html_string: generated HTML
    
    """
    
    html_string = ""
    #adds enctype of a file upload is included in the form.
    if 'file' in type_dict.values():
        html_string +=  """<form role=form action = {0} method = "POST" enctype=multipart/form-data > <div class="form-group">""".format(url)
    else:
         html_string += """<form role=form action = {0} method = "POST"> <div class="form-group">""".format(url)
       
    for key in k:
        
        html_string += "<label>  {0} </label> <br>".format(beautifyName(key))
        
        if type_dict[key] == "obj":
            #need an object [TODO]
            pass
        
        if default_dict[key] != '_':
            #this has defaults
                default = default_dict[key]
                if '=' in default:
                    default = default.strip().split('=')[1].strip()
        else:
            default = ""
                    
        
        if options_dict[key] != '_' and '|' in options_dict[key]:
            #It has options then use a Dropbox
            options = options_dict[key]
            if '=' in options:
                options = options.split('=')[1].strip().split('|')
            else:
                options = options.strip().split('|')
            
            options = [x.strip() for x in options]
            print options
            
            
            html_string += ' <select  class="form_control" name = "' + str(key) + '">'
            for option in options:
                if option == default:
                    html_string += '<option value = "' + str(option) + '" selected >' + str(option) + '</option>'

                else:
                    html_string += '<option value = "' + str(option) + '">' + str(option) + '</option>'
            
            html_string += '</select><br><br>'

            

        
            
        else:
            #then either int / float / string
            t = type_dict[key]
            try:
                x = default 
            except:
                default = " "
            if t.strip() == 'int':
                html_string += """<input type="number" class="form-control" name="{0}" value = "{1}"> <br>""".format(key,default)
                
            elif t.strip() == 'float':
                html_string += """<input type="number" class="form-control" name="{0}" value = "{1}"> <br>""".format(key,default)

            
            elif t.strip() == 'str' or t.strip() == 'string':  #correct this to str
                html_string += """<input type="text" class="form-control" name="{0}" value = "{1}"> <br>""".format(key,default)

            elif t.strip() == 'file':
                html_string += """<input type="file" class="form-control" name="{0}" value = "{1}"> <br>""".format(key,default)

                
    html_string += """<button type="submit" class="btn btn-default">  Submit </button></div></form>"""
    return html_string




def htmlToPopUpHTML(html,modal_title = None , modal_name = "modal1"):
    
    """
    Creates a strandard bootstrap HTML into a popup form.
    
    Parameters
    ----------
    html: the html component.
    title: title of the popup, default = None
    
    Returns
    -------
    popUpHTML: the popup html code
    script: javascript code to make the html "pop"
    
    """
    
    popUpHTML =  """<div class="modal fade" id="{0}" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title" id="myModalLabel">{1}</h4>
                    </div>
                    <div class="modal-body">
                      {2}
                    </div>
                    <!-- <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Save changes</button>
                    </div> -->
                  </div>
                </div>
              </div>
              """.format(modal_name, modal_title, html)
    script = """<script type="text/javascript">
            $(window).load(function(){
                $('#modal1').modal('show');
            });
        </script>"""
    return popUpHTML, script

def demonstrate(html, script = " ", file_name = "demo_html.html"):
    """
    Creates a simple html file to demo the generated html (and script)
    """
    
    f = open(file_name, "w")
    before = """<!DOCTYPE html>
<html lang="en">
<head>



	<meta charset="UTF-8">
	<title>pyBootstrapHTML Demonstration</title>

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	

	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootswatch/3.2.0/cerulean/bootstrap.min.css">
	
</head>
<body>"""

    after = """</body>
</html>"""

    html_string = before + html + script + after
    f.write(html_string)
    f.close()
    

    
    print "File saved as " , file_name


def getAlert(color, message):
    """
    creates a bootstrap alert.
    
    Parameters
    ----------
    color: color of the alert
    message: the message text. This could be plain text , or html text.
    
    Returns
    --------
    html_string: the html code for the alert.
    
    """
    #Change this accordingly
    alert_match = {'orange':"alert-warning", "red":"alert-danger", "green":"alert-success", "blue":"alert-info"}
    alert_type = alert_match[color]
    
    
    html_string = """<div class="alert alert-dismissable {0} "><button type="button" class="close" data-dismiss="alert">x</button><p class="text-center">{1}</p></div>""".format(alert_type,message)
    return html_string





def dftoBHtml(df, show_select = False, view_func = "url1", id='example1', btn_name = 'Select',key_column_pos = 0, hide_column_pos = [] ):

    """
    Takes in either a list of lists or a pandas dataframe and crates a bootstrap html table. The line breaks will be converted into html breaks.
    
    Parameters
    ----------
    df: list of lists OR pandas dataframe
    show_select: bool, if True, displays a select button for each row.
    view_func: the url the form be POSTed. (the id variable will be POSTed - see key_column_pos)
    id: the id of the table. Required if you want to conver the table into a jquery datatable.
    btn_name: name of the "select" button
    key_column_pos: the position of the column (within the list) to be passed when the "select" button is pressed
    
    Returns
    -------
    
    s: string, html code for the table
    
    
    """

    if type(df) == list:
        list_of_lists = df
        #then its a list of lists
        
        try:
            link = url_for(view_func) #this will work if you run it within flask. [TODO] remove
        except:
            link = view_func
            
        s = '' #html string
        s += """<table border=0 id="{0}" class='display compact' cellspacing="0" width="100%" >""".format(id)
        
        #processing the header
        #first list is the header
        header = list_of_lists[0]
        s+= "<thead>"
        s+="<tr>"
        colCount = -1
        for column in header:
            colCount = colCount + 1
            if colCount in hide_column_pos:
                #hide this cell / column
                continue
            s+= "<th>" + str(column) + "</th>"
        if show_select:
            s+= "<th>" + "Select" + "</th>" #select column header
        s+= "</tr>"
        s+= "</thead>"
        
        #processing the data section
        #remove header from the list_of_lists
        list_of_lists = list_of_lists[1:]
        
        s+= "<tbody>"
        
        for row in list_of_lists:
            s+= "<tr>"
            colCount = -1
            for cell in row:
                colCount = colCount + 1
                if colCount in hide_column_pos:
                    #hide this cell / column
                    continue
                #convert line breaks to <br>
                if type(cell) == str and "\n" in cell:
                    cell = cell.replace("\n", '<br>')
                    
                s+= "<td>" + str(cell) + "</td>"
            if show_select:
                #select button [TODO] replace str('x')
                s+= """<td><form role=form action = """ + link+ """ method = "POST"> 
                                <input type="hidden" name="id" value='""" +  row[key_column_pos] + """'>
                                <input type="submit" value = ' {0}' class="btn btn-info btn-xs">  
                            </form></td>""".format(btn_name)
            s+= "</tr>"
            
        s+= "</tbody>"
        s+= "</table>"

        

    else:
        #if a pandas dataframe is recieved
        
        try:
            link = url_for(view_func) #this will work if you run it within flask. [TODO] remove
        except:
            link = view_func
        
        s = '' #html string
        
        #class='table table-striped
        s += """<table border = 0 id = "{0}" class='display compact' cellspacing="0" width="100%" >""".format(id)
        
        #processing the header
        s+= "<thead>"
        s+="<tr>"
        for column in df.columns:
            s+= "<th>" + str(column) + "</th>"
        if show_select:
            s+= "<th>" + "Select" + "</th>" #select column header

        s+= "</tr>"
        s+= "</thead>"
        #processing the data section
        
        s+= "<tbody>"
        for row_number in range(len(df)):
            s+= "<tr>"
            key  = df.ix[row_number][key_column_pos]
            for column_item in df.ix[row_number]:
    
                if type(column_item) == str and "\n" in column_item:
                    column_item.replace("\n", '<br>')
                s+= "<td>" + str(column_item) + "</td>"
            
            if show_select:
                #select button
                s+= """<td><form role=form action = """ + link+ """ method = "POST">
                                <input type="hidden" name="id" value='""" +  str(key) + """'>
                                <input type="submit" value = '{0}' class="btn btn-info btn-xs">  
                            </form></td>""".format(btn_name)
            s+= "</tr>"
            
        s+= "</tbody>"
        s+= "</table>"

    return s

def dictToTable(d, bootstrap = False, header = ["key", "value"]):
    """
    Takes in a dict and creates an HTML table from it.
    
    
    
    d: dictionary containing data
    bootstrap: is the returned table a bootstrap table
    header: the header values
    
    Returns
    -------
    
    html_string: table html code
    """
    
    #check to see if d is indeed a dict
    
    try:
        x = dict(d)
    except:
        return d #simply returns the same value
    
    html_string = ""
    if bootstrap:
        html_string += "<table border = 0 class = 'table'>"
    else:
        html_string += "<table border = 0>"
        
        
    html_string += "<tr>"
    html_string += "<th>"
    html_string += str(header[0])
    html_string += "</th>"
        
    html_string += "<th>"
    html_string += str(header[1])
    html_string += "</th>"
    html_string += "</tr>"  
        
    for key in d:
        html_string += "<tr>"
        
        html_string += "<td>"
        html_string += str(key)
        html_string += "</td>"
        
        html_string += "<td>"
        try:
            html_string += str(d[key])
        except:
            html_string += " " 
        html_string += "</td>"
        
        html_string += "</tr>"
    html_string += "</table>"
    return html_string
    
    

