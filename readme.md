##pyBootstrapHTML ##

### Helper functions for using python to create bootstrap HTML code###




#### Current Version : 1.2####

#####TODO#####

* ~~forms~~
* form validation / warnings
* ~~alerts~~
* navbar
* blockquote
* block level buttons
* ~~tables~~
* pills
* bread crumbs
* labels
* badges
* progressbar
* groupstabs
* panel
* wells





###Introduction###
[Bootstrap](http://getbootstrap.com) is a lifesaver for artistically challenged programmers (such as myself) to create acceptable looking web pages. (The gifted ones use bootstrap to get stunning results but that's a different story!) I wrote pyBootStrapHTML to simply the creation of bootstrap HTML using python. 

Version 1 deals only with the creation of HTML forms. Version 2 will have the capability to create other bootstrap components (menu bars, tables etc.)

###How TO###

#### Version 1.2: bootstrap tables####
pyBootstrapHTML can create tables using either lists of lists or pandas dataframes. The tables could either be standad tables or will have select buttons for each row.

~~~python
import pyBootstrapHTML
list_of_lists = [["car", "mpg"], ["model1", 20], ["model2", 20], ["model3", 20], ["model4", 18],["model5", 1],["model2", 5]]
html_string = pyBootstrapHTML.dftoBHtml(list_of_lists)

~~~

That's it!

~~~html
<table border=1 id="example1" class='display compact' cellspacing="0" width="100%" ><thead><tr><th>car</th><th>mpg</th></tr></thead><tbody><tr><td>model1</td><td>20</td></tr><tr><td>model2</td><td>20</td></tr><tr><td>model3</td><td>20</td></tr><tr><td>model4</td><td>18</td></tr><tr><td>model5</td><td>1</td></tr><tr><td>model2</td><td>5</td></tr></tbody></table>

~~~
To visualize...
~~~python
pyBootstrapHTML.demonstrate(html_string)
~~~
![screenshot](https://bitbucket.org/xcommay/pybootstrap/raw/master/table1.png)


Alternatively you can add a select button that will post one of the cell values as id when clicked. You can select the name on the button (btn_name) and which column is the id (key_column_pos).

~~~python
import pyBootstrapHTML
list_of_lists = [["car", "mpg"], ["model1", 20], ["model2", 20], ["model3", 20], ["model4", 18],["model5", 1],["model2", 5]]


html_string = pyBootstrapHTML.dftoBHtml(list_of_lists show_select = True, view_func = "url1", id='example1', btn_name = 'Select',key_column_pos = 0 )

pyBootstrapHTML.demonstrate(html_string)
~~~

The above will give you ...

![screenshot](https://bitbucket.org/xcommay/pybootstrap/raw/master/table2.png)

This can be easily integrated with [jquery datatables](http://www.datatables.net) to add sorting and search functionality. (link the datatables with the id)







####Version 1.1: bootstrap alerts

pyBootstrapHTML can automatically create alerts. You simply need to specify the color of the alert and the text. (Note: Calliberate the color using the alert_match dictionary.)

~~~python
import pyBootstrapHTML
color = "red"
message = "This is a red message"
html_string = pyBootstrapHTML.getAlert(color, message)
~~~

Thats it!. html_string contains the code for the alert.

~~~html
<div class="alert alert-dismissable alert-danger "><button type="button" class="close" data-dismiss="alert">x</button><p class="text-center">This is a red message</p></div>
~~~

To test it let's use the demonstrate function.

~~~python
pyBootstrapHTML.demonstrate(html_string)
~~~

You should see something like this.

![screenshot](https://bitbucket.org/xcommay/pybootstrap/raw/master/alert.png)

####Version 1.0 : text to HTML forms####

The main input here is a string which describes each field in the form. The string can have any of the following format.



    i. var_name: description [ age : age of the person]
    ii. var_name: type, description [age: int, age of person]
    iii. var_name: type, default value, description [age: int, default = 40, age of person ]
    iv. var_name: type, default value, range, description [age: int, default = 40, 0 to 100, age of person]
    v. var_name, type, range, description [age:int,  0 to 100, age of person]
    v1. var_name, default, options, description [age: default = 40, options = 10 | 20 | 30 | 40, age of person]


For example:
~~~
"project_name: string, Name of the project
 projectlet: string, Name of the local Network
 description: string, Project Description" 
~~~

If the above text is used then the form will have fields for project_name, projectlet and description. pyBootstrapHTML will automatically beautify the names . e.g. project-name --> Project Name.

~~~python
import pyBootstrapHTML

form_text = """project_name: string, Name of the project
    projectlet: string, Name of the local Network
    description: string, Project Description"""
    
html,script = pyBootstrapHTML.textToBHtmlForm(text = form_text ,url = "/demo", popup = True, modal_title = "Title of Popup", modal_name = "modal1")

#url is the usr the form is POSTed.
#popup = True makes the form as a popup. 

#if you do not want to make the form a popup
html = pyBootstrapHTML.textToBHtmlForm(text = form_text ,url = "/demo", popup = False, modal_title = "Title of Popup", modal_name = "modal1")

~~~

That's it. In the html variable you will have the bootstrap HTML code. If popup == True, then the script variable will have the javascript code to make the popup. Simpley inject these variables into your html page.

html will look like this.
~~~html
<div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title" id="myModalLabel">None</h4>
                    </div>
                    <div class="modal-body">
                      <form role=form action = www method = "POST"> <div class="form-group"><label>  Project Name </label> <br><input type="text" class="form-control" name="project_name" value = " "> <br><label>  Projectlet </label> <br><input type="text" class="form-control" name="projectlet" value = " "> <br><label>  Description </label> <br><input type="text" class="form-control" name="description" value = " "> <br><button type="submit" class="btn btn-default">  Submit </button></div></form>
                    </div>
                    <!-- <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Save changes</button>
                    </div> -->
                  </div>
                </div>
              </div>
~~~

If you want to see check if the code looks alright use...

~~~python
pyBootstrapHTML.demonstrate(html, script = " ", file_name = "demo_html.html")

#or if popup == False

pyBootstrapHTML.demonstrate(html = html, script = " ", file_name = "demo_html.html"

#an html file will be saved. Open it with any webbrowser.
~~~

You will see something like this...
![screenshot](https://bitbucket.org/xcommay/pybootstrap/raw/master/screenshot.png)

Thats it!. Enjoy pyBootstrapHTML. 
(Hopefuly I will be able to release Version 2 soon. Current end date: mid August 2014.)
